import javafx.scene.layout.GridPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import java.util.Random;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;

public class Board
{
  private int mineAmount;
  private int MAXSIZE = 14;
  private Random rand;
  private GridPane grid;
  private ToggleButton[][] board;
  private int[][] posMineDisValue;

  public Board(Label announcement)
  {
      rand = new Random(System.currentTimeMillis());
      mineAmount = rand.nextInt((int)((MAXSIZE * MAXSIZE) * 0.3) - 10) + 10;
      grid = new GridPane();

      DrawandCreateBoard(announcement);
      InitalizeMineDisValue();
      PlaceMines();
      CalculateMineDistances();

      grid.setAlignment(Pos.BOTTOM_CENTER);
  }

  public GridPane getLayout()
  {
    return grid;
  }

  public int getSize()
  {
    return MAXSIZE;
  }

  public Random getRandom()
  {
    return rand;
  }

  public void DrawandCreateBoard(Label announcement)
  {
    board = new ToggleButton[MAXSIZE][MAXSIZE];
    for(int x = 0; x < MAXSIZE; x++)
    {
      for(int y = 0; y < MAXSIZE; y++)
      {
        ToggleButton b = new ToggleButton(" ");
        ApplyButtonEvents(b, x, y, announcement);
        b.setMinWidth(30);
        b.setMinWidth(30);
        board[x][y] = b;
        grid.add(b, x, y);
      }
    }
  }

  //disable hover effecting opacity

  public void ApplyButtonEvents(ToggleButton b, int x, int y, Label announcement)
  {
    b.setOnMousePressed(new EventHandler<MouseEvent>()
    {
      @Override
      public void handle(MouseEvent event)
      {
        if(event.getButton() == MouseButton.PRIMARY)
        {
          ///IF F OR ?
          if(posMineDisValue[x][y] == -1)
          {
            b.setSelected(true);
            DisplayMines(announcement);
          }
          else if(posMineDisValue[x][y] == 0)
          {
            //b.setDisable(true);
            //b.setOpacity(1);
            ShowAllNearBy(x, y);
          }
          else
          {
            b.setText(Integer.toString(posMineDisValue[x][y]));
            b.setSelected(true);
            b.setDisable(true);
            b.setOpacity(1);
          }
        }
        else if(event.getButton() == MouseButton.SECONDARY && b.isSelected() != true)
        {
          if(b.getText() == "F")
          {
            b.setText("?");
          }
          else if(b.getText() == "?")
          {
            b.setText(" ");
          }
          else
          {
            b.setText("F");
          }
        }
        if(CheckIfWon() == mineAmount)
        {
          announcement.setText("Congratulations you won!!");
        }
      }
    });
  }

  public void InitalizeMineDisValue()
  {
    posMineDisValue = new int[MAXSIZE][MAXSIZE];
    for(int x = 0; x < MAXSIZE; x++)
    {
      for (int y = 0; y < MAXSIZE; y++)
      {
        posMineDisValue[x][y] = 0;
      }
    }
  }

  public void GenerateCoordinates(Random r, int xCor, int yCor)
  {
    do
    {
      xCor = r.nextInt(MAXSIZE);
      yCor = r.nextInt(MAXSIZE);
    }while(posMineDisValue[xCor][yCor] == -1);
    posMineDisValue[xCor][yCor] = -1;
  }

  public void PlaceMines()
  {
    int xCor = 0;
    int yCor = 0;
    Random r = new Random(System.currentTimeMillis());
    for(int amount = 0; amount < mineAmount; amount++)
    {
      GenerateCoordinates(r, xCor, yCor);
    }
  }

  public int GetMineNumber(int xCor, int yCor)
  {
    int count = 0;
    int xTemp = xCor - 1;
    int yTemp;

    while(xTemp != xCor + 2)
    {
      yTemp = yCor - 1;
      while(yTemp != yCor + 2)
      {
        if(xTemp >= 0 && xTemp < MAXSIZE && yTemp >= 0 && yTemp < MAXSIZE && posMineDisValue[xTemp][yTemp] == -1)
        {
          count++;
        }
        yTemp++;
      }
      xTemp++;
    }
    return count;
  }

  public void CalculateMineDistances()
  {
    for(int x = 0; x < MAXSIZE; x++)
    {
      for(int y = 0; y < MAXSIZE; y++)
      {
        if(posMineDisValue[x][y] != -1)
        {
          posMineDisValue[x][y] = GetMineNumber(x, y);
        }
      }
    }
  }

  public void ShowAllNearBy(int xCor, int yCor)
  {
    int xTemp = xCor - 1;
    int yTemp;

    while(xTemp != xCor + 2)
    {
      yTemp = yCor - 1;
      while(yTemp != yCor + 2)
      {
        if(xTemp >= 0 && xTemp < MAXSIZE && yTemp >= 0 && yTemp < MAXSIZE)
        {
          if(posMineDisValue[xTemp][yTemp] == 0)
          {
            if(board[xTemp][yTemp].isDisabled() == false)
            {
              board[xTemp][yTemp].setDisable(true);
              board[xTemp][yTemp].setSelected(true);
              board[xTemp][yTemp].setOpacity(1);
              ShowAllNearBy(xTemp, yTemp);
            }
          }
          else
          {
            board[xTemp][yTemp].setText(Integer.toString(posMineDisValue[xTemp][yTemp]));
            board[xTemp][yTemp].setDisable(true);
            board[xTemp][yTemp].setSelected(true);
            board[xTemp][yTemp].setOpacity(1);
          }
        }
        yTemp++;
      }
      xTemp++;
    }
  }

  public void DisplayMines(Label announcement)
  {
    announcement.setText("Sorry you lost");
    for(int x = 0; x < MAXSIZE; x++)
    {
      for(int y = 0; y < MAXSIZE; y++)
      {
        if(posMineDisValue[x][y] == -1)
        {
          board[x][y].setText("M");
        }
        //board[xTemp][yTemp].setSelected(true);
        board[x][y].setDisable(true);
        board[x][y].setOpacity(1);
      }
    }
  }

  public int CheckIfWon()
  {
    int amount = 0;
    for(int x = 0; x < MAXSIZE; x++)
    {
      for(int y = 0; y < MAXSIZE; y++)
      {
        if(board[x][y].isDisabled() == false && posMineDisValue[x][y] != -1)
        {
          return 0;
        }
        else if(board[x][y].getText() == "F" && posMineDisValue[x][y] == -1)
        {
          amount++;
        }
      }
    }
    return amount;
  }

  public void resetBoard(int mineA)
  {
    mineAmount = mineA;
    for(int x = 0; x < MAXSIZE; x++)
    {
      for(int y = 0; y < MAXSIZE; y++)
      {
        board[x][y].setDisable(false);
        board[x][y].setSelected(false);
        board[x][y].setText(" ");
      }
    }
  }

}
