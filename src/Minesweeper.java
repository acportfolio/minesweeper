import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class Minesweeper extends Application
{
  public static void main(String[] args)
  {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage)
  {
    primaryStage.setTitle("Minesweeper");

    Button playAgain = new Button("Play Again");
    Label announcement = new Label();
    BorderPane bp = new BorderPane();
    VBox userCont = new VBox();
    Board b = new Board(announcement);
    int MAXSIZE = b.getSize();

    playAgain.setOnAction(new EventHandler<ActionEvent>()
    {
      @Override
      public void handle(ActionEvent event)
      {
        announcement.setText("");
        int mineAmount = b.getRandom().nextInt((int)((MAXSIZE * MAXSIZE) * 0.3) - 10) + 10;
        b.resetBoard(mineAmount);
        b.InitalizeMineDisValue();
        b.PlaceMines();
        b.CalculateMineDistances();
        bp.setBottom(b.getLayout());
      }
    });

    //playAgain.setDisable(true);
    //userCont.getChildren().addAll(playAgain, announcement);
    bp.setAlignment(playAgain, Pos.CENTER);
    bp.setAlignment(announcement, Pos.CENTER);
    bp.setTop(playAgain);
    bp.setCenter(announcement);
    bp.setBottom(b.getLayout());

    Scene scene = new Scene(bp, 30*MAXSIZE, 30*MAXSIZE);
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}
