# minesweeper

Minesweeper game, written in java using javafx.

## Build
`javac -cp src src/*.java`

## Run
`$java -cp src Minesweeper`
